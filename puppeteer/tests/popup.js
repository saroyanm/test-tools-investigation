const {closeBrowser, getBrowser, openOptionsPage, startBrowserLoadExtension} = require("../common");
const {ok} = require("assert");
let frame;
let page;

async function wait(time = 300)
{
  return frame.waitFor(time);
}

async function isElementVisible(selector)
{
  return await frame.$eval(selector, (elem) => {
      return window.getComputedStyle(elem).getPropertyValue("display") !== "none"
  });
}

async function filterListTableHas(filterName)
{
  const columns= [];
  for (const columnHandle of await frame.$$("#all-filter-lists-table [data-display='originalTitle']")) {
    columns.push(await frame.evaluate(el => el.textContent, columnHandle));
  }
  return columns.includes(filterName);
}

async function switchTab(tab)
{
  return frame.click(`#sidebar [href='#${tab}']`);
}

async function addLanguage(label)
{
  const langElems = await frame.$$("#languages-boxpopup li");
  for (const langElem of langElems) {
    const hasLabel = await frame.evaluate((el, label) => el.textContent == label, langElem, label);
    if (hasLabel)
      return langElem.click();
  }
  return false;
}

async function whitelisted(label)
{
  return !!(await frame.$(`#whitelisting-table [aria-label="${label}"]`));
}

async function toggleCustomization(pref)
{
  return frame.click(`#customize [data-pref="${pref}"] button`);
}

async function toggleFilterList(label)
{
  return frame.click(`#all-filter-lists-table [aria-label="${label}"] io-toggle button`);
}

async function clickFilterListGear(label)
{
  return frame.click(`#all-filter-lists-table [aria-label="${label}"] io-popout`);
}

async function clickAbout()
{
  return frame.click("footer [data-action='open-dialog']");
}

async function closeAbout()
{
  return frame.click("[aria-labelledby='dialog-title-about'] [data-action='close-dialog']");
}

describe("Options page", () =>
{
  before(async function()
  {
    this.timeout(4000);
    const browser = await startBrowserLoadExtension("./adblockpluschrome");
    const firstPage = (await browser.pages())[0];
    await firstPage.waitFor(500);
    page = await openOptionsPage();
    await firstPage.waitFor(300);
    const frames = await page.frames();
    // puppeteer api can't access elements inside iframe.
    frame = frames.find(f => f.url().includes("desktop-options"));
    await wait();
  });

  it("Enabling 'Block additional tracking'", async function()
  {
    this.timeout(10000);
    const easyPrivacy = "https://easylist-downloads.adblockplus.org/easyprivacy.txt";
    await frame.click(`#recommend-protection-list-table [data-access='${easyPrivacy}'] button`);
    await wait(1000);
    ok(await isElementVisible("#tracking-warning"), "Should show warning if 'Allow Acceptable Ads' selected");
    await switchTab("advanced");
    await wait(6000);
    ok(await filterListTableHas("EasyPrivacy"), "Should Add EasyPrivacy to Filter list table");
    await switchTab("general");
    await wait();
  });

  it("Disabling 'Block additional tracking'", async function()
  {
    this.timeout(4000);
    const easyPrivacy = "https://easylist-downloads.adblockplus.org/easyprivacy.txt";
    await frame.click(`#recommend-protection-list-table [data-access='${easyPrivacy}'] button`);
    await wait(1000);
    ok(!(await isElementVisible("#tracking-warning")), "Should hide warning Acceptable Ads warning");

    await switchTab("advanced");
    await wait(1000);
    ok(!(await filterListTableHas("EasyPrivacy")), "Should Add EasyPrivacy to Filter list table");
    await switchTab("general");
    await wait();
  });

  it("Adding 'Deutsch + English' using 'LANGUAGE' section in General tab", async function()
  {
    this.timeout(10000);
    await frame.click("#languages-boxlabel");
    await wait(1000);
    await addLanguage("Deutsch + English");
    await wait(500);
    await switchTab("advanced");
    await wait(2000);
    ok(await filterListTableHas("EasyList Germany+EasyList"), "Should Add 'EasyList Germany+EasyList' to Filter list table");
    await switchTab("general");
    await wait();
  });

  it("Deleting 'Deutsch + English' using 'LANGUAGE' section in General tab", async function()
  {
    this.timeout(10000);
    await frame.click("#blocking-languages-table [aria-label='Deutsch + English'] .delete");
    await wait(500);
    await switchTab("advanced");
    await wait(1000);
    ok(!(await filterListTableHas("EasyList Germany+EasyList")), "Should Add 'EasyList Germany+EasyList' to Filter list table");
    await switchTab("general");
    await wait();
  });

  it("Adding website in 'Whitelisted websites' using Click and Enter key", async function()
  {
    this.timeout(10000);
    await switchTab("whitelist");
    await wait();
    await frame.focus("#whitelisting-textbox");
    await page.keyboard.type("youtube.com");
    await wait(1000);
    await frame.click("#whitelisting-add-button");
    ok(await whitelisted("youtube.com"), "'youtube.com' should be whitelisted");
    await wait(500);
    await frame.focus("#whitelisting-textbox");
    await page.keyboard.type("example.com");
    await wait(500);
    await page.keyboard.press("Enter");
    await wait();
    ok(await whitelisted("youtube.com"), "'youtube.com' should be whitelisted");
  });

  it("Poking here and there", async function()
  {
    this.timeout(20000);
    await switchTab("advanced");
    await toggleCustomization("show_statsinicon");
    await wait(1000);
    await toggleCustomization("shouldShowBlockElementMenu");
    await wait(1000);
    await toggleCustomization("show_devtools_panel");
    await wait(1000);

    await toggleFilterList("ABP filters");
    await wait(1000);
    await toggleFilterList("EasyList");

    await toggleFilterList("ABP filters");
    await wait(1000);
    await toggleFilterList("EasyList");
    await wait(1000);

    await clickFilterListGear("ABP filters");
    await wait(1000);
    await clickFilterListGear("EasyList");
    await wait(1000);

    await clickAbout();
    await wait(1000);
    await closeAbout();
    await wait(1000);

    await frame.click("#rating");
    await wait(1000);
  });

  after(async() =>
  {
    await closeBrowser();
  });
});
