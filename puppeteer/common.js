const BROWSER_PATH = process.env.BROWSER_PATH;
const puppeteer = BROWSER_PATH ? require("puppeteer-core") : require("puppeteer");
let browser;

async function startBrowserLoadExtension(extensionPath)
{
  const launch = {
    headless: false, // extension are allowed only in the head-full mode
    args: [
      `--disable-extensions-except=${extensionPath}`,
      `--load-extension=${extensionPath}`
    ],
    defaultViewport: null
  };
  if (BROWSER_PATH)
    launch.executablePath = BROWSER_PATH;

  browser = await puppeteer.launch(launch);
  return browser;
}

async function openOptionsPage()
{
  const targets = await browser.targets();
  const extensionTarget = targets.find(({ _targetInfo }) =>
  {
    return _targetInfo.title === "Adblock Plus development build";
  });
  const extensionUrl = await extensionTarget._targetInfo.url || '';
  const [,, extensionID] = extensionUrl.split('/');
  const optionsPage = "options.html";
  
  const page = await browser.newPage();
  await page.goto(`chrome-extension://${extensionID}/${optionsPage}`);
  return page;
}

async function closeBrowser()
{
  await browser.close();
}

function getBrowser()
{
  return browser;
}

module.exports = {startBrowserLoadExtension, openOptionsPage, closeBrowser,
  getBrowser};

