# test-tools-investigation

This repository is for investigating and comparing different End-to-End testing tools for testing browser extensions.

The purpose is to find simplest one among popular that can be used not only by developers.

Several candidates worth comparing:
- Selenium
- Puppeteer
- Cypress

Things to check:
- Can load extension
- Can interact with popup

What we are looking for
- Simplicity of writing tests?

## Puppeteer

```bash
npm test # Running test in Chromium
BROWSER_PATH="BROWSER PATH" npm test # Running test in locally installed chromium based browser
```

MAC OS locally installed browser paths:

- Microsoft Edge - `/Applications/Microsoft Edge Beta.app`
- Vivaldi - `/Applications/Vivaldi.app/Contents/MacOS/Vivaldi`
- Firefox dev - `/Applications/Firefox Developer Edition.app/Contents/MacOS/firefox`
